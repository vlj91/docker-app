FROM vlj91/base:latest

MAINTAINER Vaughan Jones <vaughan.jones@nbcuni.com>

RUN mkdir -pv /var/www/app

CMD tail -f /dev/null